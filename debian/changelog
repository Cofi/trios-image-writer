trios-image-writer (0.9.0) UNRELEASED; urgency=medium

  [Filip Danilovic]
  * [GTK/CLI] Drop the needless quoting from conditionals
  * [GTK/CLI] Drop the needless usage of "cat"
  * [CLI] Move sudo prompt to new line && exit properly when writing is done
  * Use bash builtin test everywhere
  * [debian/control] Optionally depend on yad-gtk2 + syntax fixes
  * Redirect "ls" stderr to log, instead of "/dev/null". Improves on 3c00d19
  * debian:
    - [copyright] Update years and format, add Milos Pavlovic,
      Add copyright for debian/* and source link
    - [control] Optionaly depend on yad-gtk2 + syntax fixes + Add VCS links
    - [compat] Bump to level 10

  [Milos Pavlovic]
  * Don't lose the "ls" stderr when no USB is inserted

 -- Filip Danilovic <filip@openmailbox.org>  Tue, 27 Feb 2018 20:18:19 +0100

trios-image-writer (0.5.1) stable; urgency=medium

  * GTK:
    - Allow to continue to drive selection without entering *sum.
    - Drop superfluous dialog.

 -- Filip Danilovic <filip@openmailbox.org>  Fri, 18 Dec 2015 20:55:18 +0100

trios-image-writer (0.5.0) stable; urgency=medium

  [Filip Danilovic]

  * debian:
    - Switch to native format.

  [Milos Pavlovic]

  * Fix drive size unit info.
  * Fix small typo.

 -- Filip Danilovic <filip@openmailbox.org>  Fri, 27 Nov 2015 22:36:46 +0100

trios-image-writer (0.4.0) stable; urgency=medium

  * Add file type filtering to ISO selection dialog.
  * Worker:
    - Try harder to acquire root permissions.
    - Use "id" to check if user is root.
    - Some syntax fixes...
  * Some syntax fixes/improvements.
  * GTK:
    - Redone main drive selection in YAD list dialog,
    - Moved old one to "List all drives". It allows to choose any drive
      found, thus enabling user to write to eSATA/Firewire etc drives ( and
      /dev/null ).
    - Fixed detection of usable drives.
    - Write to log which drive selection dialog is being used.
    - Allow goind back from "Safety check" dialog.
    - Fix "Ok" button icons in both drive selection dialogs.
  * Worker:
    - Added icon and somewhat more informative text in "Finish" dialog.

 -- Filip Danilovic <filip@openmailbox.org>  Sat, 14 Nov 2015 01:16:55 +0100

trios-image-writer (0.3.1) stable; urgency=medium

  * Update .desktop file.

 -- Filip Danilovic <filip@openmailbox.org>  Wed, 14 Oct 2015 01:21:03 +0200

trios-image-writer (0.3.0) testing; urgency=medium

  * Detect lsblk version, and set options accordingly.
  * Add SHA algorithms checksum verification.
  * Add ability to refresh devices list.
  * Do not run size check and lsblk on /dev/null, describe it instead.
  * Titles/icons added/fixed together with various UI tweaks.
  * Enable logging.
  * Added CLI version.
  * Move listing of all devices to separate dialog.
  * Try harder to avoid listing non-usable USB devices.
  * Be more verbose in log/term output about devices found.
  * Do not show ok button in drive select dialog, if no USB devices are found.
  * debian/*.install
    - Fix paths.
  * debian/control:
    - Add xclip to depends.
  * debian/*.postinst:
    - Removed.

 -- Filip Danilovic <filip@openmailbox.org>  Fri, 25 Sep 2015 23:00:56 +0200

trios-image-writer (0.2.1) testing; urgency=medium

  * Exit after calling worker.

 -- Filip Danilovic <filip@openmailbox.org>  Sun, 02 Aug 2015 21:59:56 +0200

trios-image-writer (0.2.0) testing; urgency=medium

  * Switch from zenity to YAD
  * Add checksum verification option. MD5 only for now.
  * Check if ISO fits on the selected drive.
  * When listing devices, list usb-only ones as well.
  * Various other improvements.

 -- Filip Danilovic <filip@openmailbox.org>  Sun, 02 Aug 2015 20:33:09 +0200

trios-image-writer (0.1.1) stable; urgency=medium

  * Add .desktop file.

 -- Filip Danilovic <filip@openmailbox.org>  Mon, 01 Jun 2015 21:03:53 +0200

trios-image-writer (0.1.0) mia; urgency=low

  * Initial release

 -- Filip Danilovic <filip@openmailbox.org>  Mon, 01 Jun 2015 01:50:00 +0100
