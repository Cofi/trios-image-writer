# trios-image-writer
Simple dd wrapper, used to burn images to USB/external drives and memory cards.
- Uses YAD for GUI dialogs and pv for progress.
- Also has a CLI version.
- Can verify hash checksums ( md5, sha*sum )

Requirements:
- YAD 0.28 or higher
- pv
- util-linux, 2.23 or newer recommended ( oldest tested and working is 2.21 )
